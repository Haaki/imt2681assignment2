package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type hookBody struct {
	_id             bson.ObjectId `json:"_id" bson"_id,omitempty`
	WebhookURL      string        `json:"webhookURL"`
	BaseCurrency    string        `json:"baseCurrency"`
	TargetCurrency  string        `json:"targetCurrency"`
	MinTriggerValue float64       `json:"minTriggerValue"`
	MaxTriggerValue float64       `json:"maxTriggerValue"`
}
type dataBody struct {
	BaseCurrency   string `json:"baseCurrency"`
	TargetCurrency string `json:"targetCurrency"`
}

type tick struct {
	Base  string `json:"base"`
	Date  string `json:"date"`
	Rates struct {
		AUD float32 `json:"AUD"`
		BGN float32 `json:"BGN"`
		BRL float32 `json:"BRL"`
		CAD float32 `json:"CAD"`
		CNY float32 `json:"CNY"`
		CZK float32 `json:"CZK"`
		DKK float32 `json:"DKK"`
		EUR float32 `json:"EUR"`
		GBP float32 `json:"GBP"`
		HKD float32 `json:"HKD"`
		HRK float32 `json:"HRK"`
		HUF float32 `json:"HUF"`
		IDR float32 `json:"IDR"`
		ILS float32 `json:"ILS"`
		INR float32 `json:"INR"`
		JPY float32 `json:"JPY"`
		KRW float32 `json:"KRW"`
		MXN float32 `json:"MXN"`
		MYR float32 `json:"MYR"`
		NOK float32 `json:"NOK"`
		NZD float32 `json:"NZD"`
		PHP float32 `json:"PHP"`
		PLN float32 `json:"PLN"`
		RUB float32 `json:"RUB"`
		SEK float32 `json:"SEK"`
		SGD float32 `json:"SGD"`
		THB float32 `json:"THB"`
		TRY float32 `json:"TRY"`
		USD float32 `json:"USD"`
		ZAR float32 `json:"ZAR"`
	} `json:"rates"`
}

type date struct {
	year  int
	month int
	day   int
}

var dbUrl string
var apiUrl string

func handleHook(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		fmt.Fprint(w, registerHook(r))
		fmt.Fprint(w, "POSTing!\n")
	case "GET":
		urlparts := strings.Split(r.URL.Path, "/")
		json.NewEncoder(w).Encode(getHook(bson.ObjectIdHex(urlparts[len(urlparts)-1])))
	case "DELETE":
		fmt.Fprint(w, "DELETEing\n")
	}
}
func handleLatest(w http.ResponseWriter, r *http.Request) {
	session, _ := mgo.Dial(dbUrl)
	match := tick{}
	found := false
	j := 0
	for !found {
		session.DB("cloudcurrency").C("currency").Find(bson.M{"date": fuseDate(splitTime(time.Now().Local().AddDate(0, 0, -j)))}).One(&match)
		if match.Base != "" {
			found = true
		}
		j++
	}
	json.NewEncoder(w).Encode(match)
}
func handleAverage(w http.ResponseWriter, r *http.Request) {

}
func handleEvaluationTrigger(w http.ResponseWriter, r *http.Request) {

}
func handleDbInit(w http.ResponseWriter, r *http.Request) {
	initDb(w, r, 7)
}
func registerHook(r *http.Request) bson.ObjectId {
	var hook hookBody
	json.NewDecoder(r.Body).Decode(&hook)
	hook._id = bson.NewObjectId()
	session, _ := mgo.Dial(dbUrl)
	session.DB("cloudcurrency").C("hooks").Insert(hook)
	return hook._id
}
func getHook(id bson.ObjectId) hookBody {
	session, _ := mgo.Dial(dbUrl)
	index := 0
	hooks := []hookBody{}
	session.DB("cloudcurrency").C("hooks").Find(bson.M{}).All(&hooks)
	for i := 0; i < len(hooks); i++ {
		if hooks[i]._id == id {
			index = i
		}
	}
	return hooks[index]
}
func initDb(w http.ResponseWriter, r *http.Request, a int) {
	session, _ := mgo.Dial(dbUrl)
	c := session.DB("cloudcurrency").C("currency")
	client := urlfetch.Client(appengine.NewContext(r)) //  Creates a client for appengine
	i, j := 0, 0

	for i < a {
		var T tick
		day := fuseDate(splitTime(time.Now().Local().AddDate(0, 0, -j)))
		page, err := client.Get(apiUrl + "/" + day)
		if err != nil {
			fmt.Fprintf(w, "AAAH!")
		}
		fmt.Fprint(w, day)
		json.NewDecoder(page.Body).Decode(&T)
		if T.Date == day {
			c.Insert(T)
			fmt.Fprint(w, T)
			i++
		}
		fmt.Fprint(w, "\n")
		j++
	}
}
func splitDate(s string) date {
	var d date
	parts := strings.Split(s, "-")
	d.year, _ = strconv.Atoi(parts[0])
	d.month, _ = strconv.Atoi(parts[1])
	d.day, _ = strconv.Atoi(parts[2])
	return d
}
func splitTime(t time.Time) date {
	var s date
	y, m, d := t.Date()
	s.year = y
	s.month = int(m)
	s.day = d
	return s
}
func fuseDate(d date) string {
	return strconv.Itoa(d.year) + "-" + strconv.Itoa(d.month) + "-" + strconv.Itoa(d.day)
}
func main() {
	apiUrl = "https://api.fixer.io"
	dbUrl = "assignment2:clouds@ds229435.mlab.com:29435/cloudcurrency"

	http.HandleFunc("/exchange/", handleHook)
	http.HandleFunc("/exchange/latest/", handleLatest)
	http.HandleFunc("/exchange/average/", handleAverage)
	http.HandleFunc("/exchange/evaluationtrigger/", handleEvaluationTrigger)
	http.HandleFunc("/exchange/dbInit/", handleDbInit)

	appengine.Main()
}
